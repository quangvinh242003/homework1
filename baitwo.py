def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6)) # gọi hàm sum và có tổng là 16. nằm trong khoảng (15,20) nên trả về 20 và in ra 20
print(sum(10, 2)) # gọi hàm sum và có tổng là 12. không nằm trong khoảng (15,20) nên in ra 12
print(sum(10, 12)) # gọi hàm sum và có tổng là 22. không nằm trong khoảng (15,20) nên in ra 22
